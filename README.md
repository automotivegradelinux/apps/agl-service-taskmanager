##AGL-task-manager

Task Manager service queries the system folder /proc to collect data on 
the processes currently running on the machine. Information such as 
process name, process ID, user, system CPU usage, user CPU usage, resident memory and state. 


## Verbs

| Name               | Description                               |
|:-------------------|:------------------------------------------|
| get_process_list   | retrieves the current processes from /proc|


## JSON response is an array of process entries each containing the values described below

| Name        | Description                                     |
|:------------|-------------------------------------------------|
| cmd         | name of each process							|
| tid         | unique process ID      							|
| euid        | process user                      				|
| scpu        | % of CPU time used by process in kernel mode    |
| ucpu        | % of CPU time used by process in user time      |
| resident_mem| amount of resident memory used                  |
| state       | state of process                   				|

